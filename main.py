import pygame
from pygame.locals import *
import time
import BasicVector
from threading import Thread


from walker import Walker
from board import Board
from renderer import *
from functions import *

square_count = (30, 30)
square_size = (20, 20)


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60000
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    walker = Walker(square_count)
    board = Board(square_count, square_size)

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("Maze")
    mainClock = pygame.time.Clock()

    simulation = True

    walker_thread = Thread(target=update_walker, args=(walker, board))
    walker_thread.start()
    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pause = not pause

        if not pause:

            if frameCount % 1 == 0:
                renderBoard(window, board, walker)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


def update_walker(walker, board):
    walk = True
    while walk:
        walk = walker.update(board)

    generateImage(board, square_size)

    print("Finished")


if __name__ == '__main__':
    simulate()
