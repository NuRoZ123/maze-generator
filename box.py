from BasicVector import Vec2


class Box:

    #     Walls
    #
    #       1
    #       __
    #   0 |    | 2
    #       --
    #       3
    #

    def __init__(self, x, y):
        self.walls = [True, True, True, True]
        self.pos = Vec2(x, y)
        self.visited = False

    def removeWall(self, i):
        self.walls[i] = False

    def addWall(self, i):
        self.walls[i] = True

